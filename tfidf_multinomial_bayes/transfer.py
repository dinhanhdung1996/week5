from xml.dom import minidom
import os
import glob
from lxml import etree as ET
from tfidf_multinomial_bayes import document

my_path = os.path.dirname(os.path.abspath(__file__))
off_path = os.path.join(my_path, "SVTT_Dataset", "*.xml")
files = glob.glob(off_path)
new_path = os.path.join(my_path, "SVTT")
doc_list = []

for file in files:
    dom = minidom.parse(file)
    base = os.path.basename(file)
    root = ET.Element("result")
    rows = dom.getElementsByTagName("row")
    for row in rows:
        fields = row.getElementsByTagName("field")
        doc = document.Document()
        doc.doc_type = fields[0].firstChild.data
        doc.string_data = fields[1].firstChild.data
        if doc in doc_list:
            continue
        doc_list.append(doc)
        new_row = ET.SubElement(root, "row")
        field1 = ET.SubElement(new_row, "field", name="catid")
        field1.text = doc.doc_type
        field2 = ET.SubElement(new_row, "field", name="VN")
        field2.text = doc.string_data
    tree = ET.ElementTree(root)
    end_path = os.path.join(new_path, base)
    tree.write(end_path, encoding="utf8", pretty_print=True)
