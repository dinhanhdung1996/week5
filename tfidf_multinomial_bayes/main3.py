from tfidf_multinomial_bayes import m_bayes
import os
import glob
from xml.dom import minidom
from tfidf_multinomial_bayes import document
import xml.etree.cElementTree as ET

dev = 5
document.Document.get_stop_words("vietnamese-stopwords-dash.txt")
my_path = os.path.dirname(os.path.abspath(__file__))
off_path = os.path.join(my_path, "SVTT", "*.xml")
doc_list = []
files = glob.glob(off_path)

for file in files:
    base = os.path.basename(file)
    dom = minidom.parse(file)
    rows = dom.getElementsByTagName("row")
    for row in rows:
        fields = row.getElementsByTagName("field")
        new_doc = document.Document()
        new_doc.doc_type = fields[0].firstChild.data
        new_doc.string_data = fields[1].firstChild.data
        doc_list.append(new_doc)

doc_list_class = {}
doc_list_part = {}

for doc in doc_list:
    if doc.doc_type not in doc_list_class:
        doc_list_class[doc.doc_type] = []
    doc_list_class[doc.doc_type].append(doc)

for type_doc in doc_list_class:
    if type_doc not in doc_list_part:
        doc_list_part[type_doc] = []
    k = 0
    j = 0
    n = len(doc_list_class[type_doc])
    part = int(n/5)
    for i in range(n):
        if j > part:
            j = 0
            k += 1
        if j == 0:
            doc_list_part[type_doc].append([])
        j += 1
        doc_list_part[type_doc][k].append(doc_list_class[type_doc][i])

print("done")
part = 0
time_count = 0
# while True:
#     doc_train_list = []
#     test_train_list = []
#     test_check_list = []
#     test_check_class = {}
#     bayes = m_bayes.Bayes()
#     stat = {}
#     if part == dev:
#         break
#     time_count += 1
#     print("Time ", time_count)
#     for type_doc in doc_list_part:
#         if type_doc not in stat:
#             stat[type_doc] = [0.0, 0.0, 0.0] #0 is test num, 1 is training num, 2 is accuracy
#         # if type_doc not in test_check_class:
#         #     test_check_class[type_doc] = 0
#         for i in range(len(doc_list_part[type_doc])):
#             if i == part:
#                 stat[type_doc][0] += len(doc_list_part[type_doc][part])
#                 for doc in doc_list_part[type_doc][part]:
#                     test_train_list.append(doc.string_data)
#                     test_check_list.append(doc.doc_type)
#                     # test_check_class[type_doc] += 1
#             else:
#                 for doc in doc_list_part[type_doc][i]:
#                     stat[type_doc][1] += 1
#                     doc_train_list.append(doc)
#                     bayes.add_doc(doc.doc_type, doc.string_data)
#     bayes.set_prob_list()
#     print("length train: ", len(doc_train_list))
#     print("length test : " , len(test_train_list))
#     result = bayes.get_list_test_text(test_train_list)
#     count = 0
#     for temp in range(len(result)):
#         if result[temp] not in test_check_class:
#             test_check_class[result[temp]] = 0
#         if result[temp] == test_check_list[temp]:
#             test_check_class[result[temp]] += 1
#             count += 1
#     for type_doc in stat:
#         stat[type_doc][2] = test_check_class[type_doc]/stat[type_doc][0] * 100
#         print("-------------------------------------------- ")
#         print("Type: " , type_doc)
#         print("Train: ", stat[type_doc][1])
#         print("Test: " , stat[type_doc][0])
#         print("Accuracy : ", stat[type_doc][2])
#         print("-------------------------------------------- ")
#     print(count)
#     print("Total : ", str(count/len(result) * 100))
#     print("----------------------------------------------------------------------------------=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
#     part += 1



print(" Best Case")
best = [0, 0, 2, 1, 3, 3, 4, 3, 3, 3, 1]
best_train_list = []
best_test_list = []
best_check_list = []
best_stat = {}
best_bayes = m_bayes.Bayes()
for type_doc in doc_list_part:
    if type_doc not in best_stat:
        best_stat[type_doc] = [0.0 , 0.0 , 0.0] #0 is length of test list # 1 is length of train list # 2 is accuracy
    for i in range(len(doc_list_part[type_doc])):
        if i == best[int(type_doc)]:
            best_stat[type_doc][0] += len(doc_list_part[type_doc][i])
            for doc in doc_list_part[type_doc][i]:
                best_test_list.append(doc.string_data)
                best_check_list.append(doc.doc_type)
        else:
            for doc in doc_list_part[type_doc][i]:
                best_bayes.add_doc(doc.doc_type, doc.string_data)
                best_stat[type_doc][1] += 1
                best_train_list.append(doc)
best_bayes.set_prob_list()
print("Train: " , len(best_train_list))
print("Test: ", len(best_test_list))
best_result = best_bayes.get_list_test_text(best_test_list)
best_test_class = {}
count = 0
for temp in range(len(best_result)):
    if best_result[temp] not in best_test_class:
        best_test_class[best_result[temp]] = 0
    if best_result[temp] == best_check_list[temp]:
        best_test_class[best_result[temp]] += 1
        count += 1
for type_doc in best_stat:
    best_stat[type_doc][2] = best_test_class[type_doc]/best_stat[type_doc][0] * 100
    print("---------------------------------")
    print("Type: ", type_doc)
    print("Train: ", best_stat[type_doc][1])
    print("Test: ", best_stat[type_doc][0])
    print("Accuracy: ", best_stat[type_doc][2])
    print("---------------------------------")
print("_________________________")
print("True: " , count)
print("total: ", str(count/len(best_result) * 100))






