from tfidf_multinomial_bayes import m_bayes
import os
import glob
from tfidf_multinomial_bayes import document
from xml.dom import minidom
import random


document.Document.get_stop_words("vietnamese-stopwords-dash.txt")
my_path = os.path.dirname(os.path.abspath(__file__))
off_path = os.path.join(my_path, "SVTT_Dataset", "*.xml")
files = glob.glob(off_path)

doc_list = []

for file in files:
    print(file)
    dom = minidom.parse(file=file)
    rows = dom.getElementsByTagName("row")
    for row in rows:
        fields = row.getElementsByTagName("field")
        doc = document.Document()
        doc.doc_type = fields[0].firstChild.data
        doc.string_data = fields[1].firstChild.data
        # doc.string_data += (" " + doc.doc_type)
        if doc in doc_list:
            continue
        doc_list.append(doc)

print("length: " , len(doc_list))
random.shuffle(doc_list)
ratio = 1/5
stop = int(len(doc_list) * ratio)
start = 0
while start + stop <= len(doc_list):
    doc_test = []
    test_check = []
    test_count = {}
    doc_train = []
    for i in range(len(doc_list)):
        if start <= i <= start+stop:
            doc_test.append(doc_list[i].string_data)
            test_check.append(doc_list[i].doc_type)
            if doc_list[i].doc_type not in test_count:
                test_count[doc_list[i].doc_type] = 1
            else:
                test_count[doc_list[i].doc_type] += 1
        else:
            doc_train.append(doc_list[i])
    bayes = m_bayes.Bayes()
    bayes.set_doc_list(doc_train)
    bayes.set_prob_list()
    result = bayes.get_list_test_text(doc_test)
    result_count = {}
    count = 0
    for i in range(len(result)):
        if result[i] == test_check[i]:
            count += 1
        if result[i] not in result_count:
            if result[i] == test_check[i]:
                result_count[result[i]] = 1
            else:
                if int(result[i]) == 1:
                    print("True: " , test_check[i], " False: " , result[i])
        else:
            if result[i] == test_check[i]:
                result_count[result[i]] += 1
            else:
                if int(result[i]) == 1:
                    print("True: " , test_check[i], " False: ", result[i])
    for type in result_count:
        print(type, " : " , result_count[type]/test_count[type] * 100)
    print("Accuracy: ",  count/(len(result)) * 100)
    start = start+stop + 1



