from xml.dom import minidom
import os
import glob
import multinomial_bayes.m_bayes
import random
import tfidf_multinomial_bayes.document
import tfidf_multinomial_bayes.m_bayes

ratio = 1/5
tfidf_multinomial_bayes.document.Document.get_stop_words("vietnamese-stopwords-dash.txt")
my_path = os.path.dirname(os.path.abspath(__file__))
end_path = os.path.join(my_path, "SVTT_Dataset", "*.xml")
files = glob.glob(end_path)
bayes = tfidf_multinomial_bayes.m_bayes.Bayes()
doc_list = []
test_check = []
for file in files:
    dom = minidom.parse(file)
    rows = dom.getElementsByTagName("row")
    for row in rows:
        fields = row.getElementsByTagName("field")
        doc_type = fields[0].firstChild.data
        doc_string = fields[1].firstChild.data
        random_number = random.uniform(0.0, 1.0)
        if random_number < ratio:
            if doc_string in doc_list:
                continue
            test_check.append(doc_type)
            doc_list.append(doc_string)
        else:
            bayes.add_doc(type_doc=doc_type, string_data=doc_string)

bayes.set_prob_list()
result = bayes.get_list_test_text(doc_list)
print(result)
count = 0
class_result = {}
class_check = {}
for item in test_check:
    if item not in class_check:
        class_check[item] = 1
    else:
        class_check[item] += 1
for item in result:
    if item not in class_result:
        class_result[item] = 1
    else:
        class_result[item] += 1

for item in class_check:
    print(item, ": " , class_result[item]/class_check[item]*100)

for i in range(len(result)):
    if result[i] == test_check[i]:
        count += 1

percent = count/len(result) * 100
print("accuracy percent : ", percent)
print("test: ", len(result))
print("train: ", bayes.count_doc)