import os
from difflib import SequenceMatcher
import math

class Document:
    stop_word = {}

    def __init__(self, string_data=None, doc_type=0):
        self.string_data = string_data
        self.doc_type = doc_type
        self.word_list = {}
        self.number_of_words = 0
        self.number_of_distinct_words = -1
        self.word_tf = {}

    @staticmethod
    def get_stop_words(file_name):
        my_path = os.path.dirname(os.path.abspath(__file__))
        of_path = os.path.join(my_path, file_name)
        with open(of_path, "r",encoding="utf8") as file:
            lines = file.readlines()
            for line in lines:
                line = line.strip()
                if line not in Document.stop_word:
                    Document.stop_word[line] = 1

    def to_words(self):
        self.word_list.clear()
        if self.string_data is None:
            return
        string_list = self.string_data.split()
        for string in string_list:
            string = string.lower()
            string = string.strip(' ?_,.\'\";<>!@#$%^&*()-+=:')
            if string in Document.stop_word or len(string) > 60:
                continue
            self.number_of_words += 1
            if string not in self.word_list:
                self.word_list[string] = 1
            else:
                self.word_list[string] += 1
        self.number_of_distinct_words = len(self.word_list)
        for a_word in self.word_list:
            self.word_tf[a_word] = self.word_list[a_word]/self.number_of_words
        return self.word_list

    def distance_to(self, other, word_list): # just for bag of words
        sum_word = 0
        if type(other) is not Document:
            return None
        for a_word in word_list:
            if a_word not in self.word_list[a_word]:
                self.word_list[a_word] = 0
            if a_word not in other.word_list[a_word]:
                other.word_list[a_word] = 0
            sum_word = math.pow(self.word_list[a_word] - other.word_list[a_word], 2)
        return math.sqrt(sum_word)


    def __lt__(self, other):
        return self.string_data < other.string_data

    def __eq__(self, other):
        return self.string_data == other.string_data

    def __le__(self, other):
        return self.string_data <= other.string_data
