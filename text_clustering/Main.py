import os
import glob
from xml.dom import minidom
import document
import text_clustering.Kmean
import random


def main():
    document.Document.get_stop_words("vietnamese-stopwords-dash.txt")
    my_path = os.path.dirname(os.path.abspath(__file__))
    off_path = os.path.join(my_path, "SVTT", "*.xml")
    files = glob.glob(off_path)
    k_means = text_clustering.Kmean.KMean(10)
    for file in files:
        print(file)
        dom = minidom.parse(file)
        rows = dom.getElementsByTagName("row")
        for row in rows:
            if random.uniform(0.0, 1.0) > 0.2:
                continue
            fields = row.getElementsByTagName("field")
            doc_type = fields[0].firstChild.data
            string_data = fields[1].firstChild.data
            k_means.add_doc(doc_type, string_data, False)
    k_means.k_mean()

main()
