import random
import math
from sklearn.feature_extraction.text import TfidfVectorizer



class Cluster:
    def __init__(self, default_size=1000):
        self.centroid = []
        self.data_vector = []
        self.size = default_size
        self.generate_centroid()

    def generate_centroid(self):
        self.centroid.clear()
        for i in range(self.size):
            self.centroid.append(random.randint(0,15))

    def distance_to(self, data_vector):
        sum_vector = 0
        for i in range(self.size):
            sum_vector += math.pow(self.centroid[i] - data_vector[1][i], 2)
        return math.sqrt(sum_vector)

    def add_data(self, data_vector):
        self.data_vector.append(data_vector)

    def calculate_centroid(self):
        temp_list = zip(*self.data_vector)
        result_list = []
        for item in temp_list:
            result_list.append(sum(item))
        self.centroid = result_list
        return self.centroid

    def statistic(self):
        stat_list = {}
        for data in self.data_vector:
            if data[0] not in stat_list:
                stat_list[data[0]] = 0
            stat_list[data[0]] += 1
        print(stat_list)

    def clear(self):
        self.data_vector.clear()