import document
import text_clustering.cluster
import numpy


class KMean:
    def __init__(self, default_k=10):
        self.doc_list = []
        self.k = default_k
        self.word_list = {}
        self.data_vector = []
        self.cluster = []

    def add_doc(self, doc_type, string_data, checking=False):
        if checking is True:
            if string_data not in self.doc_list:
                return
        self.data_vector.append([int(doc_type), []])
        doc = document.Document()
        doc.string_data = string_data
        doc.doc_type = doc_type
        doc.to_words()
        self.doc_list.append(doc)
        for a_word in doc.word_list:
            if a_word not in self.word_list:
                self.word_list[a_word] = 0
            self.word_list[a_word] += 1

    def vectorization(self):
        for a_word in self.word_list:
            for i in range(len(self.doc_list)):
                if a_word not in self.doc_list[i].word_list:
                    self.data_vector[i][1].append(0)
                else:
                    self.data_vector[i][1].append(self.doc_list[i].word_list[a_word])

    def k_mean(self):
        self.vectorization()
        print(1)
        for i in range(self.k):
            cluster = text_clustering.cluster.Cluster(len(self.word_list))
            self.cluster.append(cluster)
        count = 0
        while count < 100:
            for cluster in self.cluster:
                cluster.clear()
            print(3)
            for data in self.data_vector:
                best_cluster = self.cluster[0]
                min_distance = 99999999999999999999999999999999999
                for cluster in self.cluster:
                    distance = cluster.distance_to(data)
                    if distance < min_distance:
                        min_distance = distance
                        best_cluster = cluster
                best_cluster.add_data(data)
            for cluster in self.cluster:
                cluster.calculate_centroid()

    def statistic(self):
        for cluster in self.cluster:
            cluster.statistic()








