import os

class Document:
    stop_word = {}
    def __init__(self, string_data=None, doc_type=0):
        self.string_data = string_data
        self.doc_type = doc_type
        self.word_list = {}
        self.number_of_words = 0
        self.number_of_distinct_words = -1
        self.word_tf = {}

    @staticmethod
    def get_stop_words(file_name):
        my_path = os.path.dirname(os.path.abspath(__file__))
        of_path = os.path.join(my_path, file_name)
        with open(of_path, "r",encoding="utf8") as file:
            lines = file.readlines()
            for line in lines:
                line = line.strip()
                if line not in Document.stop_word:
                    Document.stop_word[line] = 1

    def to_words(self):
        self.word_list.clear()
        if self.string_data is None:
            return
        string_list = self.string_data.split()
        for string in string_list:
            string = string.lower()
            if string in Document.stop_word:
                continue
            self.number_of_words += 1
            if string not in self.word_list:
                self.word_list[string] = 1
            else:
                self.word_list[string] += 1
        self.number_of_distinct_words = len(self.word_list)
        # for a_word in self.word_list:
        #     self.word_tf[a_word] = self.word_list[a_word]/self.number_of_words
        return self.word_list
