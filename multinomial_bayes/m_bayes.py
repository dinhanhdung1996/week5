from multinomial_bayes import document
import math


class Bayes:
    def __init__(self):
        self.doc_list = {} # store all documents
        self.prob_list = {} # store the probabilty of a word of training doc to a class
        self.class_word_list = {} # store the word with the number occurs in a class (5+1)
        self.word_list = {} # store word with the number of docs it occurs (6)
        self.sum_words_class = {}
        self.total_distinct_words = 0 # store the number of distincts words in all training docs (6)
        self.count_doc = 0
        self.list_result = []

    def add_doc(self, type_doc, string_data):
        if type_doc not in self.doc_list:
            self.doc_list[type_doc] = []
        if type_doc not in self.class_word_list:
            self.class_word_list[type_doc] = {}
        check = False
        for data in self.doc_list[type_doc]:
            if string_data == data.string_data:
                check = True
                break
        if not check:
            doc = document.Document()
            doc.doc_type = type_doc
            doc.string_data = string_data
            doc.to_words()
            self.doc_list[type_doc].append(doc)
            for a_word in doc.word_list:
                if a_word not in self.word_list:
                    self.word_list[a_word] = 1
                else:
                    self.word_list[a_word] += 1
                if a_word not in self.class_word_list[type_doc]:
                    self.class_word_list[type_doc][a_word] = doc.word_list[a_word]
                else:
                    self.class_word_list[type_doc][a_word] += doc.word_list[a_word]
            self.count_doc += 1

    def get_sum_words_class(self, type_doc): # (8)
        if type_doc in self.sum_words_class:
            return self.sum_words_class[type_doc]
        if type_doc not in self.class_word_list:
            print("unavailable")
            return
        else:
            get_sum = 0
            for doc in self.doc_list[type_doc]:
                get_sum += doc.number_of_words
            self.sum_words_class[type_doc] = get_sum
            return get_sum

    def get_number_of_distinct(self):
        return len(self.word_list)

    def set_doc_list(self, doc_list):
        if type(doc_list) is not document.Document:
            print("Wrong value data")
            return
        for item in self.doc_list:
            for text in self.doc_list[item]:
                self.add_doc(item, text)

    def get_doc_list(self):
        return self.doc_list

    @staticmethod
    def to_doc(doc_type, string):
        if type(string) is not str:
            print("Wrong data")
            return
        doc = document.Document()
        doc.doc_type = doc_type
        doc.string_data = string
        doc.to_words()
        return doc

    # def set_prob_list(self):
    #     for target in self.doc_list:
    #         list_doc = self.doc_list[target]
    #         for item in list_doc:
    #             if item.doc_type not in self.prob_list:
    #                 self.prob_list[item.doc_type] = {}
    #             else:
    #                 for a_word in item.word_list:# self.doc_list[item]:
    #                     if a_word in self.prob_list[item.doc_type]:
    #                         continue
    #                     self.prob_list[item.doc_type][a_word] = (self.class_word_list[item.doc_type][a_word] + 1) / \
    #                                                             (self.get_number_of_distinct() +
    #                                                                 self.get_sum_words_class(item.doc_type))

    def set_prob_list(self):
        for item in self.class_word_list:
            if item not in self.prob_list:
                self.prob_list[item] = {}
            for a_word in self.word_list:
                if a_word not in self.class_word_list[item]:
                    self.class_word_list[item][a_word] = 0
                self.prob_list[item][a_word] = (self.class_word_list[item][a_word] + 0.05) / \
                                                (self.get_number_of_distinct()*0.05 + self.get_sum_words_class(item))

    def get_test_text(self, string_data):
        doc_test = document.Document()
        doc_test.string_data = string_data
        doc_test.to_words()
        class_result = {}
        for a_word in doc_test.word_list:
            for item in self.prob_list:
                if item not in class_result:
                    class_result[item] = math.log(len(self.doc_list[item])/self.count_doc)
                else:
                    if a_word not in self.prob_list[item]:
                        continue
                    else:
                        class_result[item] += (math.log(self.prob_list[item][a_word]) * doc_test.word_list[a_word])
        best_value = -99999999999999999
        best_name = None
        for item in class_result:
            if class_result[item] > best_value:
                best_value = class_result[item]
                best_name = item
        return best_name

    def get_list_test_text(self, list_string):
        self.list_result = []
        for string_data in list_string:
            self.list_result.append(self.get_test_text(string_data))
        return self.list_result







